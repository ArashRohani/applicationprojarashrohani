﻿using System;

namespace TransportBusiness
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }

        protected virtual int MoveStep { get; } = 1;

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;

            // change the car destination after passenger picking up with the passenger destination position
            AssignDestination(passenger.DestinationXPos, passenger.DestinationYPos);
        }

        public void Move(int pointX, int pointY)
        {
            if (XPos < pointX)
            {
                MoveRight();
            }
            else if (XPos > pointX)
            {
                MoveLeft();
            }
            else if (YPos < pointY)
            {
                MoveUp();
            }
            else if (YPos > pointY)
            {
                MoveDown();
            }
        }

        public void Move()
        {
            Move(DestinationXPos, DestinationYPos);
        }

        public bool PickedUpPassenger()
        {
            return Passenger != null;
        }

        public bool CanPickUp()
        {
            return XPos == DestinationXPos && YPos == DestinationYPos;
        }


        public void AssignDestination(int pointX, int pointY)
        {
            DestinationXPos = pointX;
            DestinationYPos = pointY;
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
