﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportBusiness
{
    public enum CarType { Sedan, RaceCar }
    
    public class CarFactory
    {
        public static Car GetCar(CarType carType, int xPos, int yPos, City city, Passenger passenger)
        {
            Car car = null;
            switch (carType)
            {
                case CarType.Sedan:
                    car = new Sedan(xPos, yPos, city, passenger);
                    break;
                case CarType.RaceCar:
                    car = new RaceCar(xPos, yPos, city, passenger);
                    break;
                default:
                    car = new Sedan(xPos, yPos, city, passenger);
                    break;
            }

            return car;
        }
    }
}
