﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportBusiness
{
    public class RaceCar : Car
    {
        protected override int MoveStep { get; } = 2;

        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos + MoveStep <= DestinationYPos)
            {
                YPos += MoveStep;
            }
            else if (YPos < City.YMax)
            {
                YPos++;
            }

            WritePositionToConsole();
        }

        public override void MoveDown()
        {
            if ( YPos - MoveStep >= DestinationYPos)
            {
                YPos -= MoveStep;
            }
            else if (YPos > 0)
            {
                YPos--;
            }
            WritePositionToConsole();
            
        }

        public override void MoveRight()
        {
            if ( XPos + MoveStep <= DestinationXPos)
            {
                XPos += MoveStep;
            }
            else if(XPos < City.XMax )
            {
                XPos++;
            }
            WritePositionToConsole();
            
        }

        public override void MoveLeft()
        {
            if (XPos - MoveStep >= DestinationXPos)
            {
                XPos -= MoveStep;
            }
            else if (XPos > 0)
            {
                XPos--;
            }
            WritePositionToConsole();

        }
    }
}
