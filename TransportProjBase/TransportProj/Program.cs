﻿using Nito.AsyncEx;
using System;
using System.Threading.Tasks;
using TransportBusiness;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncContext.Run(() => MainAsync(args));
        }

        static async Task MainAsync(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            // assign a passenger to a car for pick up
            car.AssignDestination(passenger.StartingXPos, passenger.StartingYPos);

            while (!passenger.IsAtDestination())
            {
                await TickAsync(car, passenger);
            }

            Console.ReadLine();
            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static async Task TickAsync(Car car, Passenger passenger)
        {
            if (car.PickedUpPassenger())
            {
                // move toward the destination 
                car.Move();
                await passenger.DownloadVeyo();
            }
            else if (car.CanPickUp())
            {
                passenger.GetInCar(car);
            }
            else
            {
                car.Move();
            }
        }
    }
}
