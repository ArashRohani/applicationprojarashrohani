﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TransportBusiness;
using System.Web.Caching;
using TransportWeb.Models;

namespace TransportWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Veyo Application";

            return View();
        }

        public JsonResult Start()
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            // assign a passenger to a car for pick up
            car.AssignDestination(passenger.StartingXPos, passenger.StartingYPos);

            HttpContext.Session.Add("City", MyCity);
            HttpContext.Session.Add("Car", car);
            HttpContext.Session.Add("Passenger", passenger);

            var result = new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            result.Data = new
            {
                CarXPos = car.XPos,
                CarYPos = car.YPos,
                PassengerXPos = passenger.StartingXPos,
                PassengerYPos = passenger.StartingYPos,
                DestinationXPos = passenger.DestinationXPos,
                DestinationYPos = passenger.DestinationYPos
            };

            return result;
        }
    }
}
