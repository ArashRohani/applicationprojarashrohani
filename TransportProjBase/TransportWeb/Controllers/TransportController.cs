﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TransportBusiness;
using TransportWeb.Models;

namespace TransportWeb.Controllers
{
    public class TransportController : ApiController
    {
        // GET api/values
        public CarModel Get()
        {
            CarModel carModel = null;
            Car car = null;
            Passenger passenger = null;
            if (HttpContext.Current.Session["Car"] != null)
            {
                car = (Car)HttpContext.Current.Session["Car"];
            }

            if (HttpContext.Current.Session["Passenger"] != null)
            {
                passenger = (Passenger)HttpContext.Current.Session["Passenger"];
            }
            if (!passenger.IsAtDestination())
            {
                if (car.PickedUpPassenger())
                {
                    // move toward the destination 
                    car.Move();
                }
                else if (car.CanPickUp())
                {
                    passenger.GetInCar(car);
                }
                else
                {
                    car.Move();
                }

                carModel = new CarModel() { XPos = car.XPos, YPos = car.YPos, HasPassenger = car.Passenger != null };
            }
            else
            {
                HttpContext.Current.Session.Remove("Car");
                HttpContext.Current.Session.Remove("Passenger");
            }
            
            return carModel;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
