﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TransportWeb.Models
{
    public class CarModel
    {
        public int XPos { get; set; }
        public int YPos { get; set; }

        public bool HasPassenger { get; set; }
    }
}