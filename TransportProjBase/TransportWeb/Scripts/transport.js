﻿(function transport() {
    var timer;

    $(document).on('click', '.startButton', function () {
        if (timer) {
            clearInterval(timer);
        }

        var ajax = $.ajax({
            url: '/home/start',
            dataType: 'json'
        }).done(function (data) {
            if (data) {
                var $city = $('.city');
                var height = parseInt($city.css('height'));
                var width = parseInt($city.css('width'));
                var header = parseInt($('.panel-heading').css('height'));

                var $car = $('.car');
                $car.show();
                $car.css('top', (height * data.CarXPos) / 10 + header);
                $car.css('left', (width * data.CarYPos) / 10 + header);

                var $passenger = $('.passenger');
                $passenger.show();
                $passenger.css('top', (height * data.PassengerXPos) / 10 + header);
                $passenger.css('left', (width * data.PassengerYPos) / 10 + header);

                var $destination = $('.destination');
                $destination.show();
                $destination.css('top', (height * data.DestinationXPos) / 10 + header);
                $destination.css('left', (width * data.DestinationYPos) / 10 + header);

                timer = setInterval(getData, 2000);
            }
        });
    });

    function getData() {
        var ajax = $.ajax({
            url: '/api/transport/',
            dataType: 'json'
        }).done(function (data) {
            if (data) {
                var $city = $('.city');
                var height = parseInt($('.city').css('height'));
                var width = parseInt($('.city').css('width'));
                var header = parseInt($('.panel-heading').css('height'));

                var $car = $('.car');
                $car.css('top', (height * data.XPos) / 10 + header);
                $car.css('left', (width * data.YPos) / 10 + header);
                if (data.HasPassenger) {
                    $('.passenger').hide();
                }
            }
            else {
                clearInterval(timer);
            }
        });
    }

})();